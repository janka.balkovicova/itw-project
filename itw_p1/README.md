# Projekt 1
Stáhněte si balík itw_p1.zip a rozbalte na disk. Balík obsahuje stránku [itw.html](itw1.html) a související obrázky.
Vytvořte stylový předpis [itw1.css](itw1.css) v jazyce CSS tak, aby výsledný vzhled dokumentu odpovídal následujícímu návrhu ve formě obrázku:
* [široké okno](assignment/itw1_wide.jpg)
* [normální okno prohlížeče](assignment/itw1_normal.jpg)
* [užší okno](assignment/itw1_medium.png)
* [velmi úzké okno](assignment/itw1_narrow.png)

_(Některé prohlížeče zobrazují obrázky zmenšené, případně kolem nich zobrazují bílé místo. Ujistěte se prosím, že vidíte obrázek v plné velikosti a případné bílé místo kolem něj ignorujte.)_

## Pokyny ke zpracování
* Dodržte jména souborů.
* Soubor [itw1.html](itw1.html) nesmí být změněn.
* Stránka se musí správně zobrazit v prohlížeči Firefox aktuální verze, ostatní prohlížeče v tomto projektu nemusíte uvažovat.
* Velikost stránky se musí plynule přizpůsobit šířce okna prohlížeče od `650px` výše. V užším okne prohlížeče se může zobrazit horizontální posuvná lišta.

## Doplňující informace o návrhu
* Výsledný styl by měl pokud možno věrně odpovídat grafickému návrhu včetně barevnosti, druhů písma a vzájemné polohy a velikosti jednotlivých prvků obsahu. Na druhou stranu není nutno pracovat s přesností na pixely.
* Balík se zadáním obsahuje veškeré obrázky, které jsou pro vytvoření stylu potřebné. **Tyto obrázky není nutné nijak upravovat (řezat apod.)**
* Použité písmo je `Arial`, orientační velikosti jsou:
    - `64px` pro hlavní titulek,
    - `24px` podtitul, 
    - `36px` nadpisy sekcí, 
    - `19px` text v úvodní sekci, 
    - `15px` malé písmo v úvodní sekci.
* Maximální šířka obsahu je `1170px` (z toho je `padding 15px` vlevo i v pravo). V šiším okně se obsah centruje na stránce, pozadí sekcí je vždy přes celou šířku okna (viz. [široké okno](assignment/itw1_wide.jpg)). V užším okně se postupně snižuje počet sloupců, sloupce se centrují (viz obrázky výše).
* Titulní sekce `.main-title` s fotografií na pozadí vždy na počátku vyplní celou plochu okna prohlížeče, při posunování stránky se posunuje nahoru, ale fotografie zůstává zafixovaná: [výchozí stav](assignment/title.png) a [posunutí](assignment/title_scroll.png). \
**Tip:** mohou se hodit délkové jednotky `vh` a vlastnost `background-size`.
* **Odkazy** nejsou podtržené, ve výchozím stavu jsou bílé, při najetí myší se přebarví na `#fd367e`. 
* Tlačítko pod titulkem se při najetí přebarví obdobně, jak je vidět z [obrázku](assignment/title_scroll.png).
* Tlačítka v ceníku mají inverzní barvy: [ukázka](assignment/button.png) (první ukazuje vzhled při najetí, ostatní v normálním stavu).

## Odevzdání
Projekt se odevzdává jako jeden archív ZIP, který obsahuje stylový předpis itw1.css a všechny použité obrázky. Jméno odevzdávaného souboru musí být váš_login.zip. Tento soubor se odevzdá přes informační systém FIT.

**Datum odevzdání:** 1.4.2019 \
**Hodnocení:** max. 20 bodů