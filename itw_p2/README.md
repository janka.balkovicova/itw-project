_Stránka je dostupná na [http://www.leafcoffee.mikita.eu/](http://www.leafcoffee.mikita.eu/)_

# Projekt 2
Vytvořte stránky pro imaginární zařízení v jedné z následujících oblastí podle vlastního uvážení:
* restaurace nebo kavárna,
* autodopravce.

_(Zvolte takový název zařízení, aby se neshodoval s názvem žádné existující organizace, kterou lze na WWW již nalézt (viz. Google).)_

Navrhněte vhodnou informační architekturu webu, která bude obsahovat: 
* nejméně **tři hlavní informační sekce** (např. základní informace, služby, kontakt), každá bude tvořena nejméně **dvěma dokumenty** (např. nákladní doprava, osobní doprava),
* úvodní stránka obsahující obecné informace a **sekci s aktualitami** (upoutávky, akce apod.),
* vhodně navržená globální navigací, která je dostupná ze všech dokumentů,
* lokální (kontextová) navigace v každé sekci, která obsahuje více dokumentů (o odkazy na jiné dokumenty ve stejné sekci, nikoliv o odkazy v rámci jednoho dokumentu),
* stálé záhlaví a zápatí,
* **mapa webu** přístupná z každé stránky (např. odkaz v zápatí).

## Grafické zpracování
* Grafický návrh zahrnuje minimálně odpovídající barevné schéma (lze použít i různé pomocné [nástroje](http://wellstyled.com/tools)) a obrázkové logo použitelné v záhlaví stránky (může se jednat i o stylizovaný text). 

* Je vhodné vylepšit vzhled pomocí fotografií odpovídajících tématu stránek. V rámci školního projektu je možné využít libovolné fotografie dostupné online. Současně mějte ale na paměti praktické aspekty:
    * _načítání obrázků může trvat déle, případně se nemusí podařit_ (dbejte na to, aby text webu byl čitelný, i když se obrázky nezobrazí, např. je-li obrázek použitý jako pozadí pod textem, použijte současně i odpovídající barevné pozadí),
    * _obrázek na pozadí obecně snižuje čitelnost textu_, věnujte pozornost tomu, aby byl text dostatečně kontrastní.

* Připravte odpovídající CSS styl stránek.
* Navíc připravte tiskovou verzi stylu, která se použije při tisku stránky z prohlížeče (lze zkontrolovat v náhledu pro tisk). Tisková verze **nebude obsahovat navigaci**, pouze obsah.

_Kvalita grafického návrhu není rozhodujícím kritériem hodnocení projektů, v extrémních případech ale může hodnocení ovlivnit (oběma směry)._

## Zvláštní požadavky
Na úvodní stránce implementujte pomocí JavaScriptu jednu oblast (box), která bude při prvním zobrazení skrytá a místo ní bude zobrazen pouze její titulek, ikona apod. Po kliknutí na ikonu se zobrazí obsah oblasti a dalším kliknutím se opět skryje. Pomocí **cookies** implementujte ukládání stavu těchto boxů tak, aby uživatel viděl při návratu na stránku boxy v takovém stavu (zobrazené nebo nezobrazené), v jakém je zanechal. \
**Typické použití:** část nabídky nebo doplňkové informace ve sloupci s nabídkou, podrobnější kontaktní údaje, podrobnější informace k údajům v textu

## Praktické zpracování
* Výchozí dokument se bude jmenovat `index.html`
* Texty použité na stránkách si vymyslete. V místech, kde lze předpokládat delší souvislý text (profil apod.) je možno použít generátor výplňkového textu ([Lorem Ipsum](http://www.lipsum.com/),  [Pseudo-čeština](http://www.wellstyled.com/tools/)). Použijte dostatečné množství textu formátovaného v souladu s předpokládaným obsahem tak, aby bylo patrné, jak bude stránka ve výsledku vypadat. Nepoužívejte souvislý text ze stránek třetích stran.

## Požadavky na technické zpracování
* Stránky realizujte pomocí **HTML 5** a **CSS**. Použití experimentálních rozšíření CSS lze použít za podmínky, že se stránka korektně zobrazí v prohlížečích zmíněných níže.
* Kód stránek musí být validní při kontrole [validátorem W3C](http://validator.w3.org).
* **Respektujte obecná [pravidla přístupnosti](http://www.pravidla-pristupnosti.cz/) webových stránek.**
* Veškeré navigační prvky musí být použitelné i bez zapnuté podpory JavaScriptu v prohlížeči. Skrývací box požadovaný výše musí být v takovém případě trvale zobrazen.
* Používejte sémantické značkování (záhlaví, sekce, nadpisy, seznamy apod.) tak, aby byl web bez problémů srozumitelný i bez podpory CSS.
* Nepoužívejte značky a atributy označené ve specifikaci jako zavržené nebo zastaralé.
* Tabulky používejte **pouze** k prezentaci tabulárních dat,
nikoliv k realizaci vizuálního rozložení stránky.

## Umístění stránek
Vytvořené stránky umístěte na libovolný server přístupný z Internetu (např. fakultní, některý freehosting, ...).
Titulek webu (záhlaví) bude fungovat jako odkaz na zvolené umístění webu (t.j. na hlavní stránku absolutním URL).

**Veškerý kód musí být vlastním dílem studenta.** Při návrhu vzhledu je možno se inspirovat existujícími stránkami, nelze však převzít žádný kód z cizích webů ani z volně dostupných knihoven. Totéž platí i pro JavaScriptový kód. Využití obecných JS knihoven (např. JQuery) je povoleno. CSS frameworky (např. Bootstrap) **nejsou** povoleny. Přestože se frameworky v praxi často využívají, cílem projektu je ukázat vlastní zvládnutí technologií probíraných v rámci ITW.

## Odevzdání
Projekt se odevzdává jako jeden archív ZIP, který obsahuje všechny soubory webu. Jméno odevzdávaného souboru musí být **_vaslogin_.zip**. Tento soubor se odevzdá přes informační systém FIT. Pokud narazíte na limit velikosti odevzdávaného souboru (cca. 2 MB), odevzdejte v archivu pouze kód bez obrázků a dalších dat. Verzi dostupnou na WWW však samozřejmě nechte kompletní.

Projekt musí být funkční minimálně v prohlížečích **Firefox**, **Chrome** a **Edge** v aktuálních verzích (běžně dostupných přes automatické aktualizace).

**Datum odevzdání:** 24.4.2019 \
**Hodnocení:** max. 30 bodů

Hodnotit se bude splnění všech výše uvedených kritérií, kvalita zpracování a použitelnost pro daný účel. Samotné splnění všech kritérií neznamená automaticky plný počet bodů. Plného počtu bodů mohou dosáhnout pouze stránky evidentně propracované a použitelné pro daný účel.