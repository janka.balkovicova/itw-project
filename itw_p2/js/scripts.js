$(function () {
    $('.article').each(function () {
        let newsHiddenCookie;
        newsHiddenCookie = readCookie('old-news-visible');
        if (newsHiddenCookie !== null) {
            $(this).parent().children('.older-switch').text('Skryť staršie');
            if ($(this).hasClass('hidden'))
                $(this).show();
        } else {
            $(this).parent().children('.older-switch').text('Zobraziť staršie');
            if ($(this).hasClass('hidden'))
                $(this).hide();
        }
    });

    $('.older-switch').on('click', function () {
        let newsHiddenCookie;
        newsHiddenCookie = readCookie('old-news-visible');
        if (newsHiddenCookie === null) {
            $(this).text('Skryť staršie');
            $(this).parent().children('.hidden').show();
            createCookie('old-news-visible', 'true', 1)
        } else {
            $(this).text('Zobraziť staršie');
            $(this).parent().children('.hidden').hide();
            eraseCookie('old-news-visible');
        }
    });
});

//How to set cookies: https://stackoverflow.com/a/1460174
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}